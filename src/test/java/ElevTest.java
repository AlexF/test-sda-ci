import static org.junit.jupiter.api.Assertions.assertEquals;

import model.Elev;
import org.junit.jupiter.api.Test;

public class ElevTest {

  Elev elev = new Elev();

  @Test
  public void checkThatElevCanWalkTest() {
    assertEquals(elev.walk(),"elevul merge pe strada");
  }

}
